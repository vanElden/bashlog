#
# MANAGED BY PUPPET
#

#'history' options
declare -rx HISTFILE="$HOME/.bash_history"          # Make these variables unchangeable
declare -rx HISTSIZE=500000                         # num of cmds in memory
declare -rx HISTFILESIZE=500000                     # num of cmds on file
declare -rx HISTCONTROL=""                          # does not ignore spaces or duplicates
declare -rx HISTIGNORE=""                           # does not ignore patterns
declare -rx HISTCMD                                 # history line number
#history -r                                         # to reload history from file if a prior HISTSIZE has truncated it

if [ "${OSTYPE:0:7}" != "solaris" ] ; then          # following not working in solaris
    if groups | grep -q root ; then
#        declare -x TMOUT=3600                       # timeout for root's sessions
        chattr +a "$HISTFILE"                       # set append-only
    fi
fi
shopt -s histappend                                 # append, don't overwrite
shopt -s cmdhist                                    # put all lines of multi-line commands into history

shopt -s histverify                                 # history substitution ask for a confirmation

#add timestamps in history - obsoleted with logger/syslog
#'http://www.thegeekstuff.com/2008/08/15-examples-to-master-linux-command-line-history/#more-130'
#declare -rx HISTTIMEFORMAT='%F %T '
