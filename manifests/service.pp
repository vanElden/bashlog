# = Class: bashlog::service
# Configures bashlog service.
class bashlog::service {
    class { 'profile_rsyslog':
        log_format                     => 'modern',
	system_log_rate_limit_interval => '0',
    }
}
