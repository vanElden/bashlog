# = Class: bashlog
# Author: Peter H. Ruegg <gitlab@incense.org>
# Description of class
#
# == Parameters
class bashlog(
        ) inherits bashlog::params {

    # Setup parameters
    
    # Run classes
    anchor {"bashlog::begin":} ~>
    class {"bashlog::config":  } ~>
    class {"bashlog::service": } ~>
    anchor {"bashlog::end": }

}

