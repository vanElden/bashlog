# = Class: bashlog::config
# Deploys configuration for bashlog module.
class bashlog::config {
    file { "${bashlog::bashconfdir}/Z50-fixhistory.sh":
        ensure => file,
        source => "${bashlog::puppet_source_path}/profile-fixhistory.sh",
        owner => root,
        group => root,
        mode => '0644',
    }
    file { "${bashlog::bashconfdir}/Z60-audituser.sh":
        ensure => file,
        source => "${bashlog::puppet_source_path}/profile-audituser.sh",
        owner => root,
        group => root,
        mode => '0644',
    }
    file { "${bashlog::bashconfdir}/Z70-bashlog.sh":
        ensure => file,
        source => "${bashlog::puppet_source_path}/profile-bashlog.sh",
        owner => root,
        group => root,
        mode => '0644',
    }
    file_line { "ssh-log-key":
        path => "/etc/ssh/sshd_config",
        line => "LogLevel VERBOSE",
        match => "^LogLevel ",
    }
    file_line { "obsolete-root-HISTCONTROL":
        line => "#HISTCONTROL=",
        path => "/root/.bashrc",
        match => '^#?HISTCONTROL=',
    }
    file_line { "obsolete-root-HISTSIZE":
        line => "#HISTSIZE=",
        path => "/root/.bashrc",
        match => '^#?HISTSIZE=',
    }
    file_line { "obsolete-root-HISTFILESIZE":
        line => "#HISTFILESIZE=",
        path => "/root/.bashrc",
        match => '^#?HISTFILESIZE=',
    }
    file_line { "obsolete-skel-HISTCONTROL":
        line => "#HISTCONTROL=",
        path => "/etc/skel/.bashrc",
        match => '^#?HISTCONTROL=',
    }
    file_line { "obsolete-skel-HISTSIZE":
        line => "#HISTSIZE=",
        path => "/etc/skel/.bashrc",
        match => '^#?HISTSIZE=',
    }
    file_line { "obsolete-skel-HISTFILESIZE":
        line => "#HISTFILESIZE=",
        path => "/etc/skel/.bashrc",
        match => '^#?HISTFILESIZE=',
    }
    #stop avahi if messages are dropped (cf. /var/log/messages with 'net_ratelimit' or 'imuxsock begins to drop')
    #update-rc.d -f avahi-daemon remove && service avahi-daemon stop
    #https://isc.sans.edu/diary/Are+you+losing+system+logging+information+%28and+don%27t+know+it%29%3F/15106
    #$SystemLogRateLimitInterval 10 
    #$SystemLogRateLimitBurst 500
    profile_rsyslog::snippet { '45-bashlog':
        ensure    => present,
        content   => "#\n#  FILE HANDLED BY PUPPET\n#\n\n# Filter duplicated messages\n\$RepeatedMsgReduction off\n\n# Enable high precision timestamps\n\$ActionFileDefaultTemplate RSYSLOG_FileFormat\n\n# Log bash audit generated log messages to file\nif \$syslogfacility-text == 'user' and \$syslogseverity-text == 'info' and \$syslogtag startswith '[audit' then /var/log/userlog.info\n\n#then drop them\n& ~\n\nif \$syslogfacility-text == 'auth' and \$msg contains 'Found matching' then /var/log/userlog.info\n\n#'http://content.hccfl.edu/pollock/aunix2/logging.htm'\n#'http://www.rsyslog.com/doc/rsyslog_conf_filter.html'",
        file_mode => '0644',
    }
    logrotate::rule {'bashlog':
	path => '/var/log/userlog.info',
	compress => true,
	rotate => 4,
	rotate_every => 'week',
	missingok => true,
	delaycompress => true,
	dateext => true,
	ifempty => false,
	sharedscripts => true,
        postrotate => "reload rsyslog >/dev/null 2>&1 || true",
    }
}
