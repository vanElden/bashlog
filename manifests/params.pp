# = Class: bashlog::params
# Defines parameters for bashlog.
class bashlog::params {
    case $::osfamily {
        'Debian': {
            $bashconfdir = "/etc/profile.d"
            $syslogdir = "/etc/rsyslog.d"
            $syslogservice = "rsyslog"
        }
        'RedHat': {
            $bashconfdir = "/etc/profile.d"
            $syslogdir = "/etc/rsyslog.d"
            $syslogservice = "rsyslog"
        }
        default: {
            fail("Operating System unsupported. Adding support in puppettemplate::params should be sufficient.")
        }
    }

###############################
## Useful defaults
    $puppet_source_path = "puppet:///modules/bashlog"
}
